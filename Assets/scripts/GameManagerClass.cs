﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManagerClass
{

    #region Singleton Implementation
    private static object locker = new object();
    private static GameManagerClass instance = null;
    private int newPlayerControl;


    public static GameManagerClass GetInstance()
    {
        lock (locker)
        {
            if (instance == null)
            {
                instance = new GameManagerClass();
            }
            return instance;
        }
    }
    #endregion

    GameObject currentRadar;
    PlayerMarkScore[] marks;
    HiddenObject[] hiddenItems;

    float totalScore=0;

    private GameManagerClass()
    {

    }

    public void setRadar(GameObject newRad)
    {
        if (currentRadar != null)
        {
            currentRadar.SetActive(false);
        }

        currentRadar = newRad;
        newRad.SetActive(true);
    }

    public void Reveal()
    {
        marks = GameObject.Find("PlayerMarks").GetComponentsInChildren<PlayerMarkScore>();
        hiddenItems = GameObject.Find("hiddenObjects").GetComponentsInChildren<HiddenObject>();
        int itemsFound = 0;

        if (currentRadar != null)
        {
            currentRadar.SetActive(false);
        }

        for (int h = 0; h < hiddenItems.Length; h++)
        {
            for (int m = 0; m < marks.Length; m++)
            {
                hiddenItems[h].CompareWithMark(marks[m]);
            }
            if (hiddenItems[h].mark != null)
            {
                itemsFound++;
            }
        }

        for (int m = 0; m < marks.Length; m++)
        {
            marks[m].rateScore();
        }

        GameMaster master = GameObject.Find("GameMaster").GetComponent<GameMaster>();

        if (itemsFound==hiddenItems.Length)
        {
            master.DelayedWin();
        }
        else{
            master.DelayedLose();
        }
    }
}
