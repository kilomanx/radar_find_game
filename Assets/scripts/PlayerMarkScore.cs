﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMarkScore : MonoBehaviour
{
    

    public HiddenObject nearestHiddenItem;
    public float sqrNearestDistance = 0;

    public Sprite wrongTexture;
    public Sprite correctTexture;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public float rateScore()
    {
        float score = Mathf.Sqrt( sqrNearestDistance);
        SpriteRenderer sprite = GetComponent<SpriteRenderer>();
        if (nearestHiddenItem != null)
        {
            score = (1 - score/ HiddenObject.CorrectMinimunDistance) * 5 + 5;
            sprite.sprite = correctTexture;
        }
        else
        {
            sprite.sprite = wrongTexture;
            score = 0;
        }
        return score;
    }
}
