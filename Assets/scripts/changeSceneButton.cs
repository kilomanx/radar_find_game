﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class changeSceneButton : MonoBehaviour
{
    public Button yourButton;
    public string sceneName;

    void Start()
    {
        Button btn = yourButton.GetComponent<Button>();
        btn.onClick.AddListener(click);
    }

    void click()
    {
        SceneChanger.changeScene(sceneName);
    }
}