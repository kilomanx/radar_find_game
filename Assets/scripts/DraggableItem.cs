﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class DraggableItem : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    public static GameObject item;    // i changed itembeingdraged to item.

    bool start = true;
    public float offsetX;
    public float offsetY; 
    //Sprite sprite;

    public void OnBeginDrag(PointerEventData eventData)
    {
        item = gameObject;
        GetComponent<CanvasGroup>().blocksRaycasts = false;
        item.GetComponent<LayoutElement>().ignoreLayout = true;
    }


    public void OnDrag(PointerEventData eventData)
    {
        
        Vector2 newPosition = new Vector2(eventData.position.x - offsetX, eventData.position.y -offsetY);
        Debug.Log("eventData " + eventData.position);
        Debug.Log("newPosition " + newPosition);
        Debug.Log("Anchor " + GetComponent<RectTransform>().anchoredPosition);

        GetComponent<RectTransform>().anchoredPosition = newPosition;

    }

    public void OnEndDrag(PointerEventData eventData)
    {
        item = null;
        GetComponent<CanvasGroup>().blocksRaycasts = true;    
    }

}