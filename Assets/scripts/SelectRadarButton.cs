﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectRadarButton : MonoBehaviour
{

    public GameObject AssignedRadar;

    private Button btn;

    // Use this for initialization
    void Start()
    {
        btn = gameObject.GetComponent<Button>();
        btn.onClick.AddListener(click);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void click()
    {
        if (AssignedRadar == null)
        {
            Debug.Log("Radar not assigned");
        }

        GameManagerClass.GetInstance().setRadar(AssignedRadar);
    }
}
