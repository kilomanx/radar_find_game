﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hiddenObjectsSound : MonoBehaviour
{

    public AudioClip objectSound;


    private AudioSource source;
    private float lowPitchRange = .5F;
    private float highPitchRange = .5F;
    private float velToVol = .2F;
    private float velocityClipSplit = 10F;

    private void Start()
    {
        //source.pitch = Random.Range(lowPitchRange, highPitchRange);
    }


    void Awake()
    {

        source = GetComponent<AudioSource>();
    }


    void ActivateSound()
    {
        source.PlayOneShot(objectSound, 1);
    }

/*void OnCollisionEnter(Collision coll)
{
    source.pitch = Random.Range(lowPitchRange, highPitchRange);
    float hitVol = coll.relativeVelocity.magnitude * velToVol;
    if (coll.relativeVelocity.magnitude < velocityClipSplit)
        source.PlayOneShot(squareSound, hitVol);
    else
        source.PlayOneShot(triangleSound, hitVol);
}*/

}