﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMaster : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void DelayedWin()
    {
        Invoke("Win", 3);
    }

    public void DelayedLose()
    {
        Invoke("Lose", 3);
    }

    public void Win()
    {
        SceneChanger.changeScene("winScene");
    }

    public void Lose()
    {
        SceneChanger.changeScene("loseScene");
    }
}
