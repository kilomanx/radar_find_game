﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class radar : MonoBehaviour
{
    public float spinSpeed = 400;
    public GameObject bar;

    // Use this for initialization
    void Start()
    {
    }

    void OnEnable()
    {
        bar.transform.rotation=Quaternion.identity;
    }

    // Update is called once per frame
    void Update()
    {
        bar.transform.Rotate(new Vector3(0,0,-spinSpeed*Time.deltaTime));
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        other.BroadcastMessage("ActivateSound",SendMessageOptions.DontRequireReceiver);
    }

}
