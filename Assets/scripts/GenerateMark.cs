﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GenerateMark : MonoBehaviour {

    public GameObject userMark;

    private Button btn;
    public Vector3 spawnPosition;
    public int maxButtons;
    private int activeButtons;
    public GameObject parent;


    // Use this for initialization
    void Start()
    {
        btn = gameObject.GetComponent<Button>();
        btn.onClick.AddListener(GenerateMarkInstance);
    }


    public void GenerateMarkInstance()
    {
        if (activeButtons < maxButtons) { 
            GameObject newUserMark = Instantiate(userMark, spawnPosition, new Quaternion(), parent.transform) as GameObject;
            activeButtons++;
        }
    }
}
