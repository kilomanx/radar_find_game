﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HiddenObject : MonoBehaviour
{
    public const float CorrectMinimunDistance = 0.5f;
    public const float sqredCorrectMinimunDistance = CorrectMinimunDistance * CorrectMinimunDistance;

    SpriteRenderer rendererComp;
    public PlayerMarkScore mark;

    // Use this for initialization
    void Start()
    {
        rendererComp = this.GetComponent<SpriteRenderer>();
        rendererComp.color = new Color(255,255,255,0);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void CompareWithMark(PlayerMarkScore obj)
    {
        if(obj.nearestHiddenItem!= null)
        {
            // mark has already a hidden object assigned, we skip this one
            return;
        }

        Vector3 dif = transform.position - obj.transform.position;
        if (dif.sqrMagnitude <= sqredCorrectMinimunDistance)
        {
            // mark is near
            if(mark == null)
            {
                // we dont have a current mark so we assign this one as is
                mark = obj;
                obj.nearestHiddenItem = this;
                obj.sqrNearestDistance = dif.sqrMagnitude;
            }
            else
            {
                // we have a curren mark, we have to check if the new mark is nearer
                if(mark.sqrNearestDistance > dif.sqrMagnitude)
                {
                    // the new mark is nearer, we unassign the previous mark and assign the new one
                    mark.nearestHiddenItem = null;
                    mark.sqrNearestDistance = 0;

                    mark = obj;
                    obj.nearestHiddenItem = this;
                    obj.sqrNearestDistance = dif.sqrMagnitude;
                }
            }
        }
    }
}
